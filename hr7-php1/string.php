<?php $judulatas = "Berlatih PHP Pertama Kali" ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $judulatas ?></title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        echo "<h3> Soal No 1</h3>";
        /* 
            SOAL NO 1
            Tunjukan dengan menggunakan echo berapa panjang dari string yang diberikan berikut! Tunjukkan juga jumlah kata di dalam kalimat 
tersebut! 

            Contoh: 
            $string = "PHP is never old";
            Output:
            Panjang string: 16, 
            Jumlah kata: 4 
        */

        $first_sentence = "Hello PHP!" ; // Panjang string 10, jumlah kata: 2
        echo " First Sentence : <Strong>" . $first_sentence . "</Strong> <br> Panjang String: <strong>" . strlen($first_sentence) . ",</strong><br> Jumlah Kata: <strong>" . str_word_count($first_sentence) . ".</strong><br><br>"; 

        
        $second_sentence = "I'm ready for the challenges"; // Panjang string: 28,  jumlah kata: 5
        echo " Second Sentence : <Strong>" . $second_sentence . "</Strong> <br> Panjang String: <strong>" . strlen($second_sentence) . ",</strong><br> Jumlah Kata: <strong>" . str_word_count($second_sentence) . ".</strong><br><br>"; 
 
        
        echo "<h3 > Soal No 2</h3>";
        /* 
            SOAL NO 2
            Mengambil kata pada string dan karakter-karakter yang ada di dalamnya. 
            
            
        */
        $string2 = "I love PHP";
        
        echo "<label>String: </label> \"$string2\" <br>";
        echo "Kata pertama: <strong>" . substr($string2, 0, 1) . "</strong><br>" ; 
        // Lanjutkan di bawah ini
        echo "Kata kedua: <strong>" . substr($string2, 2, 4) . "</strong><br>" ; 
        echo "Kata Ketiga: <strong>" . substr($string2, 7, 3) . "</strong><br>" ; 

        echo "<h3> Soal No 3 </h3>";
        /*
            SOAL NO 3
            Mengubah karakter atau kata yang ada di dalam sebuah string.
        */
        $string3 = "PHP is old but Good!";
        echo "String: <strong> \"$string3\" </strong> <br>"; 
        // OUTPUT : "PHP is old but awesome"
        echo " Diubah Menjadi: <strong style=\"color:Tomato;\"> \"" . str_replace("Good!","awesome",$string3) . "\" </strong>";

    ?>
</body>
</html>